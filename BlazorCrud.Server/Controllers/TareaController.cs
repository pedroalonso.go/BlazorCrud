﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using BlazorCrud.Server.Models;
using BlazorCrud.Shared;
using Microsoft.EntityFrameworkCore;

namespace BlazorCrud.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TareaController : ControllerBase
    {
        private readonly DbcrudBlazorContext _dbContext;

        public TareaController(DbcrudBlazorContext DBcontext)
        {
            _dbContext = DBcontext;
        }

        [HttpGet]
        [Route("Lista")]
        public async Task<IActionResult> Lista()
        {
            var responseApi = new ResponseAPI<List<TareaDTO>>();
            var listaTareaDTO = new List<TareaDTO>();

            try
            {
                foreach (var item in await _dbContext.Tareas.ToListAsync()) //foreach (var item in await _dbContext.Tareas.Include(d=>d.).ToListAsync())
                {
                    listaTareaDTO.Add(new TareaDTO
                    {
                        Id = item.Id,
                        Nombre = item.Nombre,
                        Fecha = item.Fecha,
                        Estado = item.Estado,
                        MetaId = item.MetaId,
                        Activa = item.Activa
                    });
                }

                responseApi.EsCorrecto = true;
                responseApi.Valor = listaTareaDTO;
            }
            catch (Exception ex)
            {
                responseApi.EsCorrecto = false;
                responseApi.Mensaje = ex.Message;
            }
            return Ok(responseApi);

        }

        [HttpGet]
        [Route("Buscar/{id}")]
        public async Task<IActionResult> Buscar(int id)
        {
            var responseApi = new ResponseAPI<TareaDTO>();
            var TareaDTO = new TareaDTO();

            try
            {
                var dbTarea = await _dbContext.Tareas.FirstOrDefaultAsync(x => x.Id == id);

                if (dbTarea != null)
                {
                    TareaDTO.Id = dbTarea.Id;
                    TareaDTO.Nombre = dbTarea.Nombre;
                    TareaDTO.Fecha = dbTarea.Fecha;
                    TareaDTO.Estado = dbTarea.Estado;
                    TareaDTO.Activa = dbTarea.Activa;
                    TareaDTO.MetaId = dbTarea.MetaId;


                    responseApi.EsCorrecto = true;
                    responseApi.Valor = TareaDTO;
                }
                else
                {
                    responseApi.EsCorrecto = false;
                    responseApi.Mensaje = "No encontrado";
                }

            }
            catch (Exception ex)
            {

                responseApi.EsCorrecto = false;
                responseApi.Mensaje = ex.Message;
            }

            return Ok(responseApi);
        }

        [HttpPost]
        [Route("Guardar")]
        public async Task<IActionResult> Guardar(TareaDTO tarea)
        {
            var responseApi = new ResponseAPI<int>();

            try
            {
                var dbTarea = new Tarea
                {
                    Nombre = tarea.Nombre,
                    Fecha = tarea.Fecha,
                    Estado = tarea.Estado,
                    MetaId = tarea.MetaId,
                    Activa = tarea.Activa
                };

                _dbContext.Tareas.Add(dbTarea);
                await _dbContext.SaveChangesAsync();

                if (dbTarea.Id != 0)
                {
                    responseApi.EsCorrecto = true;
                    responseApi.Valor = dbTarea.Id;
                }
                else
                {
                    responseApi.EsCorrecto = false;
                    responseApi.Mensaje = "No guardado";
                }

            }
            catch (Exception ex)
            {

                responseApi.EsCorrecto = false;
                responseApi.Mensaje = ex.Message;
            }

            return Ok(responseApi);
        }

        [HttpPut]
        [Route("Editar/{id}")]
        public async Task<IActionResult> Editar(TareaDTO tarea, int id)
        {
            var responseApi = new ResponseAPI<int>();

            try
            {

                var dbTarea = await _dbContext.Tareas.FirstOrDefaultAsync(e => e.Id == id);

                if (dbTarea != null)
                {

                    dbTarea.Nombre = tarea.Nombre;
                    dbTarea.Fecha = tarea.Fecha;
                    dbTarea.Estado = tarea.Estado;
                    dbTarea.MetaId = tarea.MetaId;
                    dbTarea.Activa = tarea.Activa;


                    _dbContext.Tareas.Update(dbTarea);
                    await _dbContext.SaveChangesAsync();


                    responseApi.EsCorrecto = true;
                    responseApi.Valor = dbTarea.Id;


                }
                else
                {
                    responseApi.EsCorrecto = false;
                    responseApi.Mensaje = "Tarea no econtrada";
                }

            }
            catch (Exception ex)
            {

                responseApi.EsCorrecto = false;
                responseApi.Mensaje = ex.Message;
            }

            return Ok(responseApi);
        }


        [HttpDelete]
        [Route("Eliminar/{id}")]
        public async Task<IActionResult> Eliminar(int id)
        {
            var responseApi = new ResponseAPI<int>();

            try
            {

                var dbTarea = await _dbContext.Tareas.FirstOrDefaultAsync(e => e.Id == id);

                if (dbTarea != null)
                {
                    _dbContext.Tareas.Remove(dbTarea);
                    await _dbContext.SaveChangesAsync();


                    responseApi.EsCorrecto = true;
                }
                else
                {
                    responseApi.EsCorrecto = false;
                    responseApi.Mensaje = "Tarea no econtrada";
                }

            }
            catch (Exception ex)
            {

                responseApi.EsCorrecto = false;
                responseApi.Mensaje = ex.Message;
            }

            return Ok(responseApi);
        }
    }
}
