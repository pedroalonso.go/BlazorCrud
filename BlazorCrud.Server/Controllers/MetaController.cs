﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using BlazorCrud.Server.Models;
using BlazorCrud.Shared;
using Microsoft.EntityFrameworkCore;

namespace BlazorCrud.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MetaController : ControllerBase
    {
        private readonly DbcrudBlazorContext _dbContext;

        public MetaController(DbcrudBlazorContext DBcontext)
        {
            _dbContext = DBcontext;
        }

        [HttpGet]
        [Route("Lista")]
        public async Task<IActionResult> Lista()
        {
            var responseApi = new ResponseAPI<List<MetaDTO>>();
            var listaMetaDTO = new List<MetaDTO>();

            try
            {
                foreach (var item in await _dbContext.Metas.ToListAsync())
                {
                    listaMetaDTO.Add(new MetaDTO
                    {
                        Id = item.Id,
                        Nombre = item.Nombre,
                        Fecha = item.Fecha,
                        Activa = item.Activa
                    });
                }

                responseApi.EsCorrecto = true;
                responseApi.Valor = listaMetaDTO;
            }
            catch (Exception ex)
            {
                responseApi.EsCorrecto = false;
                responseApi.Mensaje = ex.Message;
            }
            return Ok(responseApi);

        }

        [HttpGet]
        [Route("Buscar/{id}")]
        public async Task<IActionResult> Buscar(int id)
        {
            var responseApi = new ResponseAPI<MetaDTO>();
            var MetaDTO = new MetaDTO();

            try
            {
                var dbMeta = await _dbContext.Metas.FirstOrDefaultAsync(x => x.Id == id);

                if (dbMeta != null)
                {
                    MetaDTO.Id = dbMeta.Id;
                    MetaDTO.Nombre = dbMeta.Nombre;
                    MetaDTO.Fecha = dbMeta.Fecha;
                    MetaDTO.Activa = dbMeta.Activa;


                    responseApi.EsCorrecto = true;
                    responseApi.Valor = MetaDTO;
                }
                else
                {
                    responseApi.EsCorrecto = false;
                    responseApi.Mensaje = "No encontrado";
                }

            }
            catch (Exception ex)
            {

                responseApi.EsCorrecto = false;
                responseApi.Mensaje = ex.Message;
            }

            return Ok(responseApi);
        }

        [HttpPost]
        [Route("Guardar")]
        public async Task<IActionResult> Guardar(MetaDTO meta)
        {
            var responseApi = new ResponseAPI<int>();

            try
            {
                var dbMeta = new Meta
                {
                    Nombre = meta.Nombre,
                    Fecha = meta.Fecha,
                    Activa = meta.Activa
                };

                _dbContext.Metas.Add(dbMeta);
                await _dbContext.SaveChangesAsync();

                if (dbMeta.Id != 0)
                {
                    responseApi.EsCorrecto = true;
                    responseApi.Valor = dbMeta.Id;
                }
                else
                {
                    responseApi.EsCorrecto = false;
                    responseApi.Mensaje = "No guardado";
                }

            }
            catch (Exception ex)
            {

                responseApi.EsCorrecto = false;
                responseApi.Mensaje = ex.Message;
            }

            return Ok(responseApi);
        }

        [HttpPut]
        [Route("Editar/{id}")]
        public async Task<IActionResult> Editar(MetaDTO meta, int id)
        {
            var responseApi = new ResponseAPI<int>();

            try
            {

                var dbMeta = await _dbContext.Metas.FirstOrDefaultAsync(e => e.Id == id);

                if (dbMeta != null)
                {

                    dbMeta.Nombre = meta.Nombre;
                    dbMeta.Fecha = meta.Fecha;
                    dbMeta.Activa = meta.Activa;


                    _dbContext.Metas.Update(dbMeta);
                    await _dbContext.SaveChangesAsync();


                    responseApi.EsCorrecto = true;
                    responseApi.Valor = dbMeta.Id;


                }
                else
                {
                    responseApi.EsCorrecto = false;
                    responseApi.Mensaje = "Meta no encontrada";
                }

            }
            catch (Exception ex)
            {

                responseApi.EsCorrecto = false;
                responseApi.Mensaje = ex.Message;
            }

            return Ok(responseApi);
        }

        [HttpDelete]
        [Route("Eliminar/{id}")]
        public async Task<IActionResult> Eliminar(int id)
        {
            var responseApi = new ResponseAPI<int>();

            try
            {

                var dbMeta = await _dbContext.Metas.FirstOrDefaultAsync(e => e.Id == id);

                if (dbMeta != null)
                {
                    _dbContext.Metas.Remove(dbMeta);
                    await _dbContext.SaveChangesAsync();


                    responseApi.EsCorrecto = true;
                }
                else
                {
                    responseApi.EsCorrecto = false;
                    responseApi.Mensaje = "Meta no econtrada";
                }

            }
            catch (Exception ex)
            {

                responseApi.EsCorrecto = false;
                responseApi.Mensaje = ex.Message;
            }

            return Ok(responseApi);
        }
    }
}
