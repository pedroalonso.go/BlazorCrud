﻿using System;
using System.Collections.Generic;

namespace BlazorCrud.Server.Models;

public partial class Tarea
{
    public int Id { get; set; }

    public string Nombre { get; set; } = null!;

    public DateTime Fecha { get; set; }

    public string Estado { get; set; } = null!;

    public int? MetaId { get; set; }

    public bool? Activa { get; set; }

    public virtual Meta? Meta { get; set; }
}
