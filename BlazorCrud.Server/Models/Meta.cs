﻿using System;
using System.Collections.Generic;

namespace BlazorCrud.Server.Models;

public partial class Meta
{
    public int Id { get; set; }

    public string Nombre { get; set; } = null!;

    public DateTime Fecha { get; set; }

    public bool? Activa { get; set; }

    public virtual ICollection<Tarea> Tareas { get; } = new List<Tarea>();
}
