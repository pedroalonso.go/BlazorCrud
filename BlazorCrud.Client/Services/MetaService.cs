﻿using BlazorCrud.Shared;
using System.Net.Http.Json;

namespace BlazorCrud.Client.Services
{
    public class MetaService : IMetaService
    {
        private readonly HttpClient _http;

        public MetaService(HttpClient http)
        {
            _http = http;
        }

        public async Task<List<MetaDTO>> Lista()
        {
            var result = await _http.GetFromJsonAsync<ResponseAPI<List<MetaDTO>>>("/api/Meta/Lista");

            if (result!.EsCorrecto)
                return result.Valor!;
            else
                throw new Exception(result.Mensaje);

        }

        public async Task<MetaDTO> Buscar(int id)
        {
            var result = await _http.GetFromJsonAsync<ResponseAPI<MetaDTO>>($"api/Meta/Buscar/{id}");

            if (result!.EsCorrecto)
                return result.Valor!;
            else
                throw new Exception(result.Mensaje);
        }

        public async Task<int> Guardar(MetaDTO meta)
        {
            var result = await _http.PostAsJsonAsync("api/Meta/Guardar", meta);
            var response = await result.Content.ReadFromJsonAsync<ResponseAPI<int>>();

            if (response!.EsCorrecto)
                return response.Valor!;
            else
                throw new Exception(response.Mensaje);
        }

        public async Task<int> Editar(MetaDTO meta)
        {
            var result = await _http.PutAsJsonAsync($"api/Meta/Editar/{meta.Id}", meta);
            var response = await result.Content.ReadFromJsonAsync<ResponseAPI<int>>();

            if (response!.EsCorrecto)
                return response.Valor!;
            else
                throw new Exception(response.Mensaje);
        }

        public async Task<bool> Eliminar(int id)
        {
            var result = await _http.DeleteAsync($"api/Meta/Eliminar/{id}");
            var response = await result.Content.ReadFromJsonAsync<ResponseAPI<int>>();

            if (response!.EsCorrecto)
                return response.EsCorrecto!;
            else
                throw new Exception(response.Mensaje);
        }
    }
}
