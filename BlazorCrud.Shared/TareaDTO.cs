﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlazorCrud.Shared
{
    public class TareaDTO
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido.")]
        public string Nombre { get; set; } = null!;

        public DateTime Fecha { get; set; }

        public string Estado { get; set; } = null!;

        [Required]
        [Range(1,int.MaxValue, ErrorMessage = "El campo {0} es requerido.")]
        public int? MetaId { get; set; }

        public bool? Activa { get; set; }

        public MetaDTO? Meta { get; set; }

        //public virtual Meta? Meta { get; set; }
    }
}
