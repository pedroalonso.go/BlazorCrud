USE [DBCrudBlazor]
GO
/****** Object:  Table [dbo].[Metas]    Script Date: 25/03/2024 03:48:47 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Metas](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](255) NOT NULL,
	[fecha] [date] NULL,
	[activa] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tareas]    Script Date: 25/03/2024 03:48:47 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tareas](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](255) NOT NULL,
	[fecha] [date] NULL,
	[estado] [varchar](255) NOT NULL,
	[meta_id] [int] NULL,
	[activa] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Metas] ON 

INSERT [dbo].[Metas] ([id], [nombre], [fecha], [activa]) VALUES (1, N'Configurar Plan de Compensación', CAST(N'2024-03-25' AS Date), 1)
SET IDENTITY_INSERT [dbo].[Metas] OFF
GO
SET IDENTITY_INSERT [dbo].[Tareas] ON 

INSERT [dbo].[Tareas] ([id], [nombre], [fecha], [estado], [meta_id], [activa]) VALUES (3, N'Tarea001', CAST(N'2024-03-25' AS Date), N'Abierta', 1, 1)
SET IDENTITY_INSERT [dbo].[Tareas] OFF
GO
ALTER TABLE [dbo].[Tareas]  WITH CHECK ADD FOREIGN KEY([meta_id])
REFERENCES [dbo].[Metas] ([id])
GO
